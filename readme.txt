The repo holds the Vagrant files to provision one ansible control server, 
four linux clients and one windows client.

To spin up the servers, clone the repository and descent in the vagrantlinux and vagrantwindows directory and run 'vagrant up'
The post installation scripts will install ansible and configure it for the systems.

You can test the configuration with logging into the control server and run


ansible linux -m ping
ansible linux2 -m ping
ansible windows -m win_ping

THe servers are provisioned with a private network and the keys are exchanged

Default passwords ( vagrant/vagrant ) are still in place.

The distribution depends on the installatin of Virtualbox and Vagrant.

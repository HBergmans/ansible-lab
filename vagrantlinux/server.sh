#!/usr/bin/bash
echo reset network interfaces
nmcli connection reload
systemctl restart network.service
sleep 3
echo configure EPEL
rpm -Uvh /vagrant/epel-release-latest-7.noarch.rpm
echo Installing Ansible
yum -y install ansible
 whoami
echo Generating keys
/usr/bin/sudo -u vagrant /vagrant/generate_key.sh
cd /home/vagrant/.ssh
echo copy key to share
cp id_rsa.pub  /vagrant
echo Import key in authorized_keys
/usr/bin/cat /vagrant/id_rsa.pub >> /home/vagrant/.ssh/authorized_keys
chown vagrant /home/vagrant/.ssh/authorized_keys
echo adding host_key_checking entry in the ansible.cfg file
echo 'host_key_checking=False' >> /etc/ansible/ansible.cfg
echo Installing pip and winrm
sudo easy_install pip
pip install "pywinrm>=0.2.2"
echo copy ssh config file
cp /vagrant/config /home/vagrant/.ssh/config
chmod 400 /home/vagrant/.ssh/config
chown vagrant /home/vagrant/.ssh/config
echo copy ansible configuration files
rm -f /etc/ansible/hosts
cp /vagrant/hosts /etc/ansible/hosts
mkdir /etc/ansible/group_vars
chmod 555 /etc/ansible/group_vars
cp /vagrant/windows /etc/ansible/group_vars
echo '192.168.3.80    windowsclient' >> /etc/hosts
